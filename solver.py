import networkx as nx

from converter import make_graph
from maze import Maze
from solution import Solution

def solve(maze: Maze) -> Solution | None:
    try:
        return Solution(
            squares=tuple(
                nx.shortest_path(
                    make_graph(maze),
                    source=maze.entrance,
                    target=maze.exit,
                    weight="weight"
                )
            )
        )
    except nx.NetworkXException:
        return None

def solve_all(maze: Maze) -> list[Solution]:
    return [
            Solution(squares=tuple(path))
            for path in nx.all_shortest_paths(
                make_graph(maze),
                source=maze.entrance,
                target=maze.exit,
                weight="weight"
            )
        ]
