import argparse
import pathlib

from solver import solve_all
from maze import Maze
from renderer import SVGRenderer

def main() -> None:
    maze = Maze.load(parse_path())
    solutions = solve_all(maze)
    
    renderer = SVGRenderer()
    for solution in solutions:
            renderer.render(maze, solution).preview()


def parse_path() -> pathlib.Path:
    parser = argparse.ArgumentParser()
    parser.add_argument("path", type=pathlib.Path)
    return parser.parse_args().path


if __name__ == "__main__":
    main()